# Blog CI Kata

Simple blog engine - java Spring Boot rest api as backend, react.js as frontend

## Backend

### Run build
```
./gradlew build
```

### Run service

Service listens http://localhost:8080/

```
./gradlew runBoot
```

## Frontend

### Install dependencies
dependencies is installing during gradle compile task, also you can do it manually:
```
npm install
```

### Build 
bundle is built during gradle compile ask, also you can do it manually
```json
npm run watch
```

